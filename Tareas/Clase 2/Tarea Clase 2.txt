Realizar un formulario similar form de la imagen sin centrarse en los estilos del mismo.
El form, además de todo lo de la imagen, también debe tener los siguientes campos:
1 - Un campo para seleccionar sexo del usuario.
2 - Un campo para seleccionar si prefiere viajar a pie, en bicicleta, auto, tren o avión.
3 - Un campo en donde acepte los términos y condiciones.
4 - Un link a los términos y condiciones.
5 - Agregar un video que se muestre al principio.
